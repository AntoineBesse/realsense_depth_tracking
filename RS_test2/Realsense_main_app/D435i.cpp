#include "Header_D435i.h"
#include "T265.cpp"


using namespace rs2;
using namespace std;
using namespace rs400;

class D435i {

public:
	float depth_scale;
	


void init(context& ctx, rs2::device& dev, vector<rs2::pipeline>& pipelines, rs2_stream& align_to) {
	cout << "camera is D435 ...";
	//create pipeline
	rs2::pipeline pipe_D435i(ctx);
	rs2::config cfg;
	//get serial number 
	cfg.enable_device(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));

	//set pipeline profile 
	rs2::pipeline_profile profile = pipe_D435i.start(cfg);
	//set depth scale 
	float depth_scale = get_depth_scale(profile.get_device());
	//set stream to align 
	align_to = find_stream_to_align(profile.get_streams());
	//rs2::align aligned(align_to);
	pipelines.emplace_back(pipe_D435i);


}

void run(rs2::pipeline& pipe, rs2::pipeline_profile& profile, rs2_stream& align_to , rs2::video_frame& other_frame,rs2::depth_frame& aligned_depth_frame) {
	// Using the align object, we block the application until a frameset is available
	rs2::frameset frameset = pipe.wait_for_frames();

	rs2::align aligned(align_to);
	//If the profile was changed, update the align object, and also get the new device's depth scale
	profile = pipe.get_active_profile();
	align_to = find_stream_to_align(profile.get_streams());
	aligned = rs2::align(align_to);
	this->depth_scale = get_depth_scale(profile.get_device());


	//Get processed aligned frame
	auto processed = aligned.process(frameset);

	 other_frame = processed.first(align_to);
	 aligned_depth_frame = processed.get_depth_frame();


}

void remove_background(rs2::video_frame& other_frame, const rs2::depth_frame& depth_frame, float depth_scale, float X_depth, float delta, uint32_t color,uint32_t color2)
{
	const uint16_t* p_depth_frame = reinterpret_cast<const uint16_t*>(depth_frame.get_data());
	uint8_t* p_other_frame = reinterpret_cast<uint8_t*>(const_cast<void*>(other_frame.get_data()));
	int width = other_frame.get_width();
	int height = other_frame.get_height();
	int other_bpp = other_frame.get_bytes_per_pixel();
	 

#pragma omp parallel for schedule(dynamic) //Using OpenMP to try to parallelise the loop
	for (int y = 0; y < height; y++)
	{
		auto depth_pixel_index = y * width;
		for (int x = 0; x < width; x++, ++depth_pixel_index)
		{
			// Get the depth value of the current pixel
			auto pixels_distance = depth_scale * p_depth_frame[depth_pixel_index];

			// Check if the depth value is invalid (<=0) or greater than the threashold
			if (pixels_distance <= 0.f || pixels_distance > X_depth * 10 || pixels_distance < (X_depth * 10)-delta)
			{
				// Calculate the offset in other frame's buffer to current pixel
				auto offset = depth_pixel_index * other_bpp;

				// Set pixel to "background" color (0x999999)
				memcpy(&p_other_frame[offset], &color, other_bpp);
			}
			else {
				auto offset = depth_pixel_index * other_bpp;
				std::memcpy(&p_other_frame[offset], &color2, other_bpp);

			}
		}
	}
}

rs2_stream find_stream_to_align(const std::vector<rs2::stream_profile>& streams)
{
	//Given a vector of streams, we try to find a depth stream and another stream to align depth with.
	//We prioritize color streams to make the view look better.
	//If color is not available, we take another stream that (other than depth)
	rs2_stream align_to = RS2_STREAM_ANY;
	bool depth_stream_found = false;
	bool color_stream_found = false;
	for (rs2::stream_profile sp : streams)
	{
		rs2_stream profile_stream = sp.stream_type();
		if (profile_stream != RS2_STREAM_DEPTH)
		{
			if (!color_stream_found)         //Prefer color
				align_to = profile_stream;

			if (profile_stream == RS2_STREAM_COLOR)
			{
				color_stream_found = true;
			}
		}
		else
		{
			depth_stream_found = true;
		}
	}

	if (!depth_stream_found)
		throw std::runtime_error("No Depth stream available");

	if (align_to == RS2_STREAM_ANY)
		throw std::runtime_error("No stream found to align with Depth");

	return align_to;
}

float get_depth_scale(rs2::device dev)
{
	// Go over the device's sensors
	for (rs2::sensor& sensor : dev.query_sensors())
	{
		// Check if the sensor if a depth sensor
		if (rs2::depth_sensor dpt = sensor.as<rs2::depth_sensor>())
		{
			return dpt.get_depth_scale();
		}
	}
	throw std::runtime_error("Device does not have a depth sensor");
}
};

