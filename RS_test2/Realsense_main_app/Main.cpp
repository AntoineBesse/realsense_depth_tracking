//////////////////////////////////////////////////////////////////
// Code Du projet avec changement de couleur					//
//////////////////////////////////////////////////////////////////

//other
#include "Header.h"
#include "D435i.cpp"
#include "config.cpp"



using namespace ci;
using namespace ci::app;
using namespace rs2;
using namespace std;
using namespace rs400;
using namespace cv;


rs2_stream align_to;
rs2::pipeline_profile profile;
rs2::frame f;
rs2::video_frame other_frame = f;
rs2::depth_frame aligned_depth_frame =f;

int main() try
{

	std::string D435_Name("Intel RealSense D435I");
	std::string T265_Name("Intel RealSense T265");
	window app(1280, 720, "RealSense depth tracking"); // Simple window handling           
	texture renderer;
	context ctx;
	std::vector<rs2::pipeline> pipelines;
	Configproject conf;
	ctx.query_devices();
	D435i camera;
	T265 capteur;
	capteur.delta = conf.distance_delta;
	

	for (rs2::device dev : ctx.query_devices())
	{
		string name = dev.get_info(RS2_CAMERA_INFO_NAME);

		if (name == D435_Name)
		{
		
			camera.init(ctx,dev,pipelines,align_to);

			//load configuration 
			auto advanced_mode_dev = dev.as<advanced_mode>();
			ifstream t(conf.url_config_d435i);
			string str((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
			advanced_mode_dev.load_json(str);


			std::cout << "done" << std::endl;

		}
		else if (name == T265_Name)
		{
			capteur.init(ctx,dev,pipelines);
			std::cout << "done" << std::endl;

		}
	}


	while (app)
	{
		
		//check pipeline T265 and D435i
		rs2::align aligned(align_to);
		rs2::pipeline pipe = pipelines.at(0);
		rs2::pipeline pipep = pipelines.at(1);
		
		capteur.run(pipep);
		
		camera.run(pipe, profile,align_to, other_frame, aligned_depth_frame);
		
		uint32_t color = conf.color_front;
		uint32_t color2 = conf.color_mask;

		

		//If one of them is unavailable, continue iteration
		if (!(aligned_depth_frame) || !other_frame)
		{
			continue;
		}
		
		//use fonction remove background to applicate the mask 
		camera.remove_background(other_frame,aligned_depth_frame, camera.depth_scale, capteur.X_depth, capteur.delta,color,color2);
		
		// Taking dimensions of the window for rendering purposes
		float w = static_cast<float>(app.width());
		float h = static_cast<float>(app.height());

		rect altered_other_frame_rect{ 0, 0, w, h };
		altered_other_frame_rect = altered_other_frame_rect.adjust_ratio({ static_cast<float>(other_frame.get_width()),static_cast<float>(other_frame.get_height()) });

		// Render aligned image
		renderer.render(other_frame, altered_other_frame_rect);

		/*if (GetAsyncKeyState(0x41)) { // si appui sur a
			cout << "T265 restart ... \n ";
			pipep.stop();
			this_thread::sleep_for(chrono::seconds(1));
			pipep.start();

		}*/
	}
	

	return EXIT_SUCCESS;
}
catch (const rs2::error & e)
{
	std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
	getchar();
	return EXIT_FAILURE;
}
catch (const std::exception & e)
{
	std::cerr << e.what() << std::endl;
	getchar();
	return EXIT_FAILURE;
}


