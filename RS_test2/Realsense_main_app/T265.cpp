#include "Header_T265.h"
#pragma once


using namespace rs2;
using namespace std;
using namespace rs400;

class T265 {

	public:
    float X_depth;
	float delta;

void init(context& ctx, rs2::device& dev, vector<rs2::pipeline>& pipelines) {

	cout << "camera is T265 ...";
	rs2::pipeline pipe_T265(ctx);
	rs2::config cfg;
	cfg.enable_device(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
	cfg.enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF);
	pipe_T265.start(cfg);
	pipelines.emplace_back(pipe_T265);

}

void run(rs2::pipeline& pipep) {

	// Wait for the next set of frames from the camera
	auto frames = pipep.wait_for_frames();
	// Get a frame from the pose streams
		auto f = frames.first_or_default(RS2_STREAM_POSE);
		// Cast the frame to pose_frame and get its data
		auto pose_data = f.as<rs2::pose_frame>().get_pose_data();
		this->X_depth = (float)pose_data.translation.x;
	

}

};