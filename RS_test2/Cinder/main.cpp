#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Capture.h"
#include "cinder/Log.h"

/*
using namespace ci;
using namespace ci::app;

class BasicApp : public App {
public:
	void draw() override;
	fs::path path = fs::path("C:/Users/Antoine/Pictures/issou.png");
	gl::TextureRef texture = gl::Texture::create(loadImage(path)); 
	//gl::Texture2dRef texture = gl::Texture2d::create(loadImage(loadAsset("")));
	
	
};

void BasicApp::draw()
{
	gl::clear();
	gl::draw(texture);
	
}

CINDER_APP(BasicApp, RendererGl)

*/

using namespace ci;
using namespace ci::app;
using namespace std;

class CaptureBasicApp : public App {
public:
	void setup() override;
	void update() override;
	void draw() override;
	void keyDown(KeyEvent event) override;
private:
	void printDevices();
	std::vector<vec2> mPoints;

	CaptureRef			mCapture;
	gl::TextureRef		mTexture;
};

void CaptureBasicApp::keyDown(KeyEvent event)
{
	if (event.getChar() == 'f') {
		// Toggle full screen when the user presses the 'f' key.
		setFullScreen(!isFullScreen());
	}
	else if (event.getCode() == KeyEvent::KEY_SPACE) {
		// Clear the list of points when the user presses the space bar.
		mPoints.clear();
	}
	else if (event.getCode() == KeyEvent::KEY_ESCAPE) {
		// Exit full screen, or quit the application, when the user presses the ESC key.
		if (isFullScreen())
			setFullScreen(false);
		else
			quit();
	}
}
void CaptureBasicApp::setup()
{
	printDevices();

	try {
		mCapture = Capture::create(640, 480);
		mCapture->start();
	}
	catch (ci::Exception &exc) {
		CI_LOG_EXCEPTION("Failed to init capture ", exc);
	}
}

void CaptureBasicApp::update()
{
	if (mCapture && mCapture->checkNewFrame()) {
		if (!mTexture) {
			// Capture images come back as top-down, and it's more efficient to keep them that way
			mTexture = gl::Texture::create(*mCapture->getSurface(), gl::Texture::Format().loadTopDown());
		}
		else {
			mTexture->update(*mCapture->getSurface());
		}
	}
}

void CaptureBasicApp::draw()
{
	gl::clear();

	if (mTexture) {
		gl::ScopedModelMatrix modelScope;
		gl::draw(mTexture);
	}
}

void CaptureBasicApp::printDevices()
{
	for (const auto &device : Capture::getDevices()) {
		console() << "Device: " << device->getName() << " " << endl;
	}
}


CINDER_APP(CaptureBasicApp, RendererGl)
