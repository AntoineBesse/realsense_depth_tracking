//////////////////////////////////////////////////////////////////////////
// Code de test de fonctionnement de la cam�ra d435i et du capteur T265	//
/////////////////////////////////////////////////////////////////////////

//other
#include "Header.h"
#include <string>
#include <iostream>

using namespace ci;
using namespace ci::app;

using namespace rs2;
using namespace std;
using namespace rs400;

void remove_background(rs2::video_frame& other, const rs2::depth_frame& depth_frame, float depth_scale, float X_depth, float delta);
float get_depth_scale(rs2::device dev);
rs2_stream find_stream_to_align(const std::vector<rs2::stream_profile>& streams);
bool profile_changed(const std::vector<rs2::stream_profile>& current, const std::vector<rs2::stream_profile>& prev);

std::string name;
rs2_stream align_to;
float depth_scale;
rs2::pipeline_profile profile;
float depth_clipping_distance = 1.f;
KeyEvent event;
float delta;

int main() try
{


	std::string D435_Name("Intel RealSense D435I");
	std::string T265_Name("Intel RealSense T265");
	window app(1280, 720, "RealSense depth tracking"); // Simple window handling
	rs2::colorizer c;                     // Helper to colorize depth images
	texture renderer;
	context ctx;
	std::vector<rs2::pipeline> pipelines;
	Configproject conf;
	delta = conf.distance_delta;
	ctx.query_devices();


	for (auto dev : ctx.query_devices())
	{
		std::string name = dev.get_info(RS2_CAMERA_INFO_NAME);

		if (name == D435_Name)
		{
			std::cout << "camera is D435 ...";
			//create pipeline
			rs2::pipeline pipe_D435i(ctx);
			rs2::config cfg;
			//get serial number 
			cfg.enable_device(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));

			//set pipeline profile 
			rs2::pipeline_profile profile = pipe_D435i.start(cfg);
			//set depth scale 
			float depth_scale = get_depth_scale(profile.get_device());
			//set stream to align 
			align_to = find_stream_to_align(profile.get_streams());
			//rs2::align aligned(align_to);
			pipelines.emplace_back(pipe_D435i);

			//load configuration 
			auto advanced_mode_dev = dev.as<advanced_mode>();


			ifstream t(conf.url_config_d435i);
			string str((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
			advanced_mode_dev.load_json(str);


			std::cout << "done" << std::endl;

		}
		else if (name == T265_Name)
		{
			cout << "camera is T265 ...";
			rs2::pipeline pipe_T265(ctx);
			rs2::config cfg;
			cfg.enable_device(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
			cfg.enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF);
			pipe_T265.start(cfg);
			pipelines.emplace_back(pipe_T265);
			std::cout << "done" << std::endl;

		}
	}

	//const char* file1 = "../image/color.jpg";
	//IplImage* image = cvLoadImage(file1, CV_LOAD_IMAGE_COLOR);

	while (app)
	{
		//check pipeline T265 and D435i
		rs2::align aligned(align_to);
		rs2::pipeline pipe = pipelines.at(0);
		rs2::pipeline pipep = pipelines.at(1);




		// Wait for the next set of frames from the camera
		auto frames = pipep.wait_for_frames();
		// Get a frame from the pose stream
		auto f = frames.first_or_default(RS2_STREAM_POSE);
		// Cast the frame to pose_frame and get its data
		auto pose_data = f.as<rs2::pose_frame>().get_pose_data();
		float X_depth = (float)pose_data.translation.x;

		//cout << X_depth * 10 << " -- " << (X_depth * 10) - delta << "\n";

		// Using the align object, we block the application until a frameset is available
		// Using the align object, we block the application until a frameset is available
		rs2::frameset frameset = pipe.wait_for_frames();

		// rs2::pipeline::wait_for_frames() can replace the device it uses in case of device error or disconnection.
		// Since rs2::align is aligning depth to some other stream, we need to make sure that the stream was not changed
		//  after the call to wait_for_frames();

		//If the profile was changed, update the align object, and also get the new device's depth scale
		profile = pipe.get_active_profile();
		align_to = find_stream_to_align(profile.get_streams());
		aligned = rs2::align(align_to);
		depth_scale = get_depth_scale(profile.get_device());


		//Get processed aligned frame
		auto processed = aligned.process(frameset);

		// Trying to get both other and aligned depth frames
		//rs2:frame_source& src = "../image.jpeg";

		rs2::video_frame other_frame = processed.first(align_to);
		rs2::depth_frame aligned_depth_frame = processed.get_depth_frame();

		//If one of them is unavailable, continue iteration
		if (!aligned_depth_frame || !other_frame)
		{
			continue;
		}
		// Passing both frames to remove_background so it will "strip" the background
		// NOTE: in this example, we alter the buffer of the other frame, instead of copying it and altering the copy
		//       This behavior is not recommended in real application since the other frame could be used elsewhere
		remove_background(other_frame, aligned_depth_frame, depth_scale, X_depth, delta);

		// Taking dimensions of the window for rendering purposes
		float w = static_cast<float>(app.width());
		float h = static_cast<float>(app.height());

		// At this point, "other_frame" is an altered frame, stripped form its background
		// Calculating the position to place the frame in the window
		rect altered_other_frame_rect{ 0, 0, w, h };
		altered_other_frame_rect = altered_other_frame_rect.adjust_ratio({ static_cast<float>(other_frame.get_width()),static_cast<float>(other_frame.get_height()) });

		// Render aligned image
		renderer.render(other_frame, altered_other_frame_rect);
		

		if (event.getChar() == 'f') {
			pipep.stop();
			pipep.start();

			cout << "appuyer ... ";
			//setFullScreen(!isFullScreen());
		}

	}


	return EXIT_SUCCESS;
}
catch (const rs2::error & e)
{
	std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
	return EXIT_FAILURE;
}
catch (const std::exception & e)
{
	std::cerr << e.what() << std::endl;
	return EXIT_FAILURE;
}

float get_depth_scale(rs2::device dev)
{
	// Go over the device's sensors
	for (rs2::sensor& sensor : dev.query_sensors())
	{
		// Check if the sensor if a depth sensor
		if (rs2::depth_sensor dpt = sensor.as<rs2::depth_sensor>())
		{
			return dpt.get_depth_scale();
		}
	}
	throw std::runtime_error("Device does not have a depth sensor");
}

void remove_background(rs2::video_frame& other_frame, const rs2::depth_frame& depth_frame, float depth_scale, float X_depth, float delta)
{
	const uint16_t* p_depth_frame = reinterpret_cast<const uint16_t*>(depth_frame.get_data());
	uint8_t* p_other_frame = reinterpret_cast<uint8_t*>(const_cast<void*>(other_frame.get_data()));

	int width = other_frame.get_width();
	int height = other_frame.get_height();
	int other_bpp = other_frame.get_bytes_per_pixel();

#pragma omp parallel for schedule(dynamic) //Using OpenMP to try to parallelise the loop
	for (int y = 0; y < height; y++)
	{
		auto depth_pixel_index = y * width;
		for (int x = 0; x < width; x++, ++depth_pixel_index)
		{
			// Get the depth value of the current pixel
			auto pixels_distance = depth_scale * p_depth_frame[depth_pixel_index];

			// Check if the depth value is invalid (<=0) or greater than the threashold
			if (pixels_distance <= 0.f || pixels_distance > X_depth * 10 || pixels_distance < (X_depth * 10) - delta)
			{
				// Calculate the offset in other frame's buffer to current pixel
				auto offset = depth_pixel_index * other_bpp;

				// Set pixel to "background" color (0x999999)
				memset(&p_other_frame[offset], 0xff, other_bpp);
			}
			
		}
	}
}

rs2_stream find_stream_to_align(const std::vector<rs2::stream_profile>& streams)
{
	//Given a vector of streams, we try to find a depth stream and another stream to align depth with.
	//We prioritize color streams to make the view look better.
	//If color is not available, we take another stream that (other than depth)
	rs2_stream align_to = RS2_STREAM_ANY;
	bool depth_stream_found = false;
	bool color_stream_found = false;
	for (rs2::stream_profile sp : streams)
	{
		rs2_stream profile_stream = sp.stream_type();
		if (profile_stream != RS2_STREAM_DEPTH)
		{
			if (!color_stream_found)         //Prefer color
				align_to = profile_stream;

			if (profile_stream == RS2_STREAM_COLOR)
			{
				color_stream_found = true;
			}
		}
		else
		{
			depth_stream_found = true;
		}
	}

	if (!depth_stream_found)
		throw std::runtime_error("No Depth stream available");

	if (align_to == RS2_STREAM_ANY)
		throw std::runtime_error("No stream found to align with Depth");

	return align_to;
}

bool profile_changed(const std::vector<rs2::stream_profile>& current, const std::vector<rs2::stream_profile>& prev)
{
	for (auto&& sp : prev)
	{
		//If previous profile is in current (maybe just added another)
		auto itr = std::find_if(std::begin(current), std::end(current), [&sp](const rs2::stream_profile& current_sp) { return sp.unique_id() == current_sp.unique_id(); });
		if (itr == std::end(current)) //If it previous stream wasn't found in current
		{
			return true;
		}
	}
	return false;
}
